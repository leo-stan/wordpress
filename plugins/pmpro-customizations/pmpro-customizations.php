<?php
/*
Plugin Name: PMPro Customizations
Plugin URI: https://www.paidmembershipspro.com/wp/pmpro-customizations/
Description: Customizations for my Paid Memberships Pro Setup
Version: .1
Author: Paid Memberships Pro
Author URI: https://www.paidmembershipspro.com
*/
 
//Now start placing your customization code below this line

function set_my_default_country_for_pmpro($default_country){
    return 'AU'; //return country code, for country code list look at - https://github.com/strangerstudios/paid-memberships-pro/blob/v1.7.8.1/includes/countries.php#L4
}

add_filter( 'pmpro_default_country', 'set_my_default_country_for_pmpro' );